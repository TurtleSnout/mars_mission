#!/usr/bin/env ruby

require './lib/robot.rb'
require './lib/mission.rb'

case ARGV[0]
when nil
	puts 'Please provide name of text file with instructions for NASA mission!'
else
	nasa_mission = Mission.new(ARGV[0])
	# nasa_mission.print_instructions unless nasa_mission.nil?

	if nasa_mission.get_number_of_robots <= 0 
		puts '[Mission status]: Mission aborted!!! No robots were defined!'
		raise ArgumentError, "No robots defined!"
	end


	File.open('output.txt', 'w') do |file| 

		# Create new robot, execute instructions and return result of operation
		for i in (0..nasa_mission.get_number_of_robots-1)	
			mars_robot = Robot.new(nasa_mission.get_robot_positions[i])
			mars_robot.execute(nasa_mission.get_robot_instructions[i])
			mars_robot.print_robot_status(nasa_mission.get_end_coordinates)
			file.write(mars_robot.return_result + "\n")
			mars_robot = nil

		end
	end
					
end


