require "test/unit"

class TestNAME < Test::Unit::TestCase

	# test if file with output was created
	def test_file_with_output_exist
		assert(File.exist?('output.txt'), "No output file was created!")
	end

	# test received result with correct output file
	def test_mission_result

		File.open('output.txt', 'r') do |correct_output| 
			File.open('./tests/correct_output.txt', 'r') do |result_output| 
				assert(FileUtils.compare_file(result_output, correct_output), "Your result is not correct!")
			end
		end

	end

end