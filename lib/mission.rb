class Mission

	def initialize(instructions_file)
		instructions = read_instructions(instructions_file)
		raise LoadError if instructions.nil?
		@instructions = instructions
		@end_coordinates, @number_of_robots, @robot_positions, @robot_instructions = decode_instructions
	end

	def print_instructions
		puts "-------------------------------"
		puts "Received instructions:"
		puts "-------------------------------"
		puts "End coordinates of surface:"
		puts get_end_coordinates.inspect
		puts "Number of robots sent to space:"
		puts get_number_of_robots
		puts "Robots coordinates:"
		puts get_robot_positions.inspect
		puts "Instructions for robots:"
		puts get_robot_instructions.inspect
		puts "-------------------------------"
		puts " "
	end

	def get_end_coordinates
		@end_coordinates.split(" ").map{ |x| x.to_i}
	end

	def get_number_of_robots
		@number_of_robots
	end

	def get_robot_positions
		@robot_positions
	end

	def get_robot_instructions
		@robot_instructions
	end


	private
	attr_accessor :instructions, :end_coordinates, :number_of_robots, :robot_positions, :robot_instructions

	# read file with input instructions for Mars mission
	def read_instructions(instructions_file)
		instructions = Array.new()
		
		begin	
			File.open('data/' + instructions_file, "r") do |f|
	  		f.each_line do |line|
	    		instructions.push(line.strip)
	  		end
	  	end

  	rescue
  		puts '[Mission status]: Critical error! File containing instructions for mission could not be read! Mission aborted!'
  		return nil
  	end

  	return instructions
  end

  # decode instructions so they can be understand by the robots
  def decode_instructions
  	end_coordinates = @instructions[0]
  	number_of_robots = (@instructions.length-1)/2
  	robot_positions = Array.new()
  	robot_instructions = Array.new()

  	for i in (1..@instructions.length-1).step(2) do
  		robot_positions.push( @instructions[i])
  		robot_instructions.push( @instructions[i+1])
  	end

  	return end_coordinates, number_of_robots, robot_positions, robot_instructions
  end

end