class Robot
	@@compass = { "N" => [0, 1],
								"E" => [1, 0],
								"S" => [0, -1],
								"W" => [-1, 0]}

	def initialize(coordinates )
		@coordinates = coordinates.split(" ").slice(0..1).map{|coordinate| coordinate.to_i}
		@direction = decode_direction(coordinates.split(" ").slice(2))
	end

	def print_robot_status(plane_edge)
		dangerous_zone?(plane_edge)
		puts return_result
	end

	def return_result
		return [@coordinates, encode_direction(@direction)].join(" ")
	end

	def execute(instruction)
		instruction.each_char do |command|
			case command
			when "L"
				turn_left
			when "R"
				turn_right
			when "M"
				move
			else
				puts "[Robot status] No such command!"
			end
		end
	end


	private

	attr_accessor :coordinates, :direction, :border_coordinates

	def dangerous_zone?(plane_edge)
		if @coordinates[0] > plane_edge[0] ||  @coordinates[1] > plane_edge[1]
			puts "[Robot status] Critical error! Robot left planet! Robot is lost!"
			return true
		end
		false
	end

	def decode_direction(cardinal_direction)
		@@compass[cardinal_direction]
	end

	def encode_direction(direction)
		@@compass.key(direction)
	end

	def turn_right
		@direction = [@direction[1], -1 * @direction[0]]
	end

	def turn_left
		@direction = [-1 *@direction[1], @direction[0]]
	end

	def move
		@coordinates[0] = @coordinates[0] + @direction[0]
		@coordinates[1] = @coordinates[1] + @direction[1]
	end
end